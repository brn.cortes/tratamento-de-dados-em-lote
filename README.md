# Tratamento de Dados em Lote

Projeto para ser entregue como atividade da matéria "Tratamento de Dados em Lote (Spark, Flink, Kafka)"

## Aplicação
O proposta da aplicação é consumir a API de tweets do Twitter, filtrando pelos tweets gerado dentro da região da Grande Florianópolis; e com base nestes tweets filtrar pelos tweets de pessoas com mais de 1.000 seguidores.

Isso pdoeria ser utilizado por exemplo para selecionar os usuários mais influentes para seguir e tentar conseguir seguidores para divulgar a sua marca.

## Fluxograma de execução
```mermaid
graph TD;
  A("[Source] fromTwitter")-- posta JSON Completo -->B>"[Queue] #quot;twitter-followers-input#quot;"]
  B-. consome JSON .->C("[Source] fromKafkaToHdfs")
  C-- escreve JSON reduzido -->CC["[HDFS] #quot;/Atividade/input#quot;"]
  C-- posta JSON reduzido -->D>"[Queue] #quot;twitter-followers-output#quot;"]
  D-. consome JSON reduzido .->E("[Source] fromKafkaAndFilter")
  E-- filtra nº seguidores -->F((filtragem))
  F-- escreve Tuple2 com registros filtrados -->H["[HDFS] #quot;/Atividade/output#quot;"]

  style A fill:#FAFAD2
  style C fill:#FAFAD2
  style F fill:#FAFAD2
  style B fill:#AFEEEE
  style D fill:#AFEEEE
  style CC fill:#98FB98
  style H fill:#98FB98
```
## Preparando o ambiente

1. Navegar até a pasta **/sbin** dentro do diretório de instalação do **Hadoop** e rodar o comando:
    1. `. start-dfs.sh`
    1. Criar os diretórios **/Atividade/input** e **/Atividade/output** no hdfs.

1. Navegar até a pasta **/bin** dentro do diretório de instalação do **Kafka** e rodar os comandos:
    1. `. zookeeper-server-start.sh ../config/zookeeper.properties`
    1. `. kafka-server-start.sh ../config/server.properties`
    1. `. kafka-topics.sh --create --topic twitter-followers-input --zookeeper localhost:2181 --partitions 1 --replication-factor 1`
    1. `. kafka-topics.sh --create --topic twitter-followers-output --zookeeper localhost:2181 --partitions 1 --replication-factor 1`

1. Devido a integração com o Hadoop, o código não funciona rodando no eclipse, é necessário rodar o jar através do **Flink Web Dashboard**; e para iniciá-lo, navegar até a pasta **/bin** dentro do diretório de instalação do **Flink** e rodar o comando:
    1. Se não estiver usando o Flink disponibilizado [aqui](https://gitlab.com/brn.cortes/tratamento-de-dados-em-lote/blob/ddde23a4025c7b20466c8953bda1cfa0a2a87334/flink-1.6.1.zip) no repositório, será necesspario copiar para a pasta **/lib** as seguintes libs usadas pelo projeto
    
        `flink-connector-filesystem_2.11-1.6.1.jar`  `guava-16.0.1.jar`
        `flink-connector-kafka-0.10_2.11-1.6.1.jar`  `hbc-core-2.2.0.jar`
        `flink-connector-kafka-0.11_2.11-1.6.1.jar`  `httpclient-4.5.6.jar`
        `flink-connector-kafka-0.9_2.11-1.6.1.jar`   `httpcore-4.4.10.jar`
        `flink-connector-kafka-base_2.11-1.6.1.jar`  `joauth-6.0.2.jar`
        `flink-dist_2.11-1.6.1.jar`                  `kafka-clients-2.0.0.jar`
        `flink-python_2.11-1.6.1.jar`                `log4j-1.2.17.jar`
        `flink-runtime_2.11-1.6.1.jar`               `slf4j-log4j12-1.7.7.jar`
        `flink-shaded-hadoop2-uber-1.6.1.jar`


    1. `. start-cluster.sh`

### Mindmap

```mermaid
graph LR;
  A(Hadoop)-->AA(Subir o HDFS)
  AA-->AAA("Criar pasta #quot;/Atividade/input#quot;")
  AA-->AAA1("Criar pasta #quot;/Atividade/output#quot;")
  B(Kafka)-->BB(Subir o Zookeeper)
  B-->BB1(Subir o Kafka)
  BB1-->BBB("Criar o tópico  #quot;twitter-followers-input#quot;")
  BB1-->BBB1("Criar o tópico  #quot;twitter-followers-output#quot;")
  C(Flink)-.->CC(Copiar as libs)
  C-->CC1(Subir o Flink)
```
