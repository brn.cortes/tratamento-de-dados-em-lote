package br.com.unisul.atividade;

import static com.twitter.hbc.core.Constants.STREAM_HOST;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.apache.flink.api.common.functions.StoppableFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;
import org.apache.flink.streaming.connectors.fs.bucketing.BasePathBucketer;
import org.apache.flink.streaming.connectors.fs.bucketing.BucketingSink;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.common.DelimitedStreamReader;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.Location;
import com.twitter.hbc.core.endpoint.Location.Coordinate;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.HosebirdMessageProcessor;
import com.twitter.hbc.httpclient.BasicClient;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

public class Atividade {

	private static transient ObjectMapper jsonParser = new ObjectMapper();

	public static void main(String[] args) throws Exception {

		final ParameterTool params = ParameterTool.fromArgs(args);

		final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
		env.setParallelism(1);

		env.getConfig().setGlobalJobParameters(params);

		DataStreamSource<String> streamFromTwitter = env.addSource(new TwitterSource(), "fromTwitter");
		DataStreamSource<String> streamToHdfs = env.addSource(consumer("twitter-followers-input"), "fromKafkaToHdfs");
		DataStreamSource<String> streamFromOutput = env.addSource(consumer("twitter-followers-output"), "fromKafkaAndFilter");

//		Usado na tentativa de ler do HDFS		
//
//		DataStreamSource<String> streamFromOutput = env.addSource(new HdfsSource(), "fromHdfs");

		SingleOutputStreamOperator<JsonNode> flatMapTwitter = streamFromTwitter.flatMap((value, out) -> {
			JsonNode jsonNode = jsonParser.readValue(value, JsonNode.class);
			if (jsonNode.has("user")) {
				sendToQueue(jsonNode, "twitter-followers-input");
			}
		});

		SingleOutputStreamOperator<String> flatMapToHdfs = streamToHdfs.flatMap((value, out) -> {
			JsonNode jsonNodeUser = jsonParser.readValue(value, JsonNode.class).get("user");
			ObjectNode user = jsonParser.createObjectNode();
			user.put("name", jsonNodeUser.get("name").textValue());
			user.put("followers", jsonNodeUser.get("followers_count").intValue());
			out.collect(user.toString());
			sendToQueue(user, "twitter-followers-output");
		});

		SingleOutputStreamOperator<Tuple2<String, Integer>> flatMapFromOutput;
		flatMapFromOutput = streamFromOutput.flatMap((value, out) -> {
			JsonNode user = jsonParser.readValue(value, JsonNode.class);
			out.collect(new Tuple2<String, Integer>(user.get("name").asText(), user.get("followers").asInt()));
			
//			Usado na tentativa de ler do HDFS, no qual ele retornava uma lista de usuários e não apenas um.
//			
//			JsonNode users = jsonParser.readValue(value, JsonNode.class);
//			users.forEach(user -> {
//				Tuple2<String, Integer> userTuple = new Tuple2<String, Integer>(user.get("name").asText(), user.get("followers").asInt());
//				out.collect(userTuple);
//			});
		});

		flatMapTwitter
				.returns(JsonNode.class);

		flatMapToHdfs
				.returns(String.class)
					.addSink(new BucketingSink<String>("/Atividade/input")
								.setBucketer(new BasePathBucketer<String>())
								.setUseTruncate(false)
								.setBatchRolloverInterval(30 * 1000))
					.name("writeInput");

		flatMapFromOutput
				.returns(new TupleTypeInfo<Tuple2<String, Integer>>(Types.STRING, Types.INT))
				.keyBy(0)
				.filter(user -> user.f1 > 1000)
				.addSink(new BucketingSink<Tuple2<String, Integer>>("/Atividade/output")
							.setBucketer(new BasePathBucketer<Tuple2<String, Integer>>())
							.setUseTruncate(false)
							.setBatchRolloverInterval(30 * 1000))
				.name("writeOutput");
		
		env.execute();
	}

	public static void sendToQueue(Object object, String topic) throws IOException {
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "localhost:9092");
		properties.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		try (KafkaProducer<String, String> producer = new KafkaProducer<>(properties)) {
			producer.send(new ProducerRecord<String, String>(topic, object.toString()));
		}
	}

	public static FlinkKafkaConsumer011<String> consumer(String topic) throws Exception {
		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", "localhost:9092");

		FlinkKafkaConsumer011<String> consumer;
		consumer = new FlinkKafkaConsumer011<>(topic, new SimpleStringSchema(), properties);
		consumer.setStartFromLatest();
		return consumer;
	}
	
	public static class TwitterSource extends RichSourceFunction<String> implements StoppableFunction {
		private transient Object waitLock;

		private transient BasicClient client;

		private transient boolean running = true;

		private static final long serialVersionUID = 7284963727210671225L;

		@Override
		public void open(Configuration parameters) throws Exception {
			waitLock = new Object();
		}

		@Override
		public void run(SourceContext<String> ctx) throws Exception {
			StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();

			Hosts hosebirdHosts = new HttpHosts(STREAM_HOST);
			Location grandeFlorianopolis = new Location(new Coordinate(-49, -28), new Coordinate(-48, -27));
			List<Location> locations = Lists.newArrayList(grandeFlorianopolis);
			endpoint.locations(locations);

			Authentication auth = new OAuth1("9LR285ECyHzJRORrBpAPLyAnj",
					"RVKX1XVeqlzO7Yrb5QR1l4vo6TzJCX6K6n7jwjbBqpQdlBVS3k",
					"122389946-kxQ6TBGgobsjTo7AX4kwhM9vS3BJpA2PEMg6tseO",
					"o7vWgpJmQySn9QBaoK7NXPMdMMEDLaP0yE0vFjLCWJEog");

			//@formatter:off
			client = new ClientBuilder()
					.name("kafka-unisul")
					.hosts(hosebirdHosts)
					.endpoint(endpoint)
					.authentication(auth)
					.processor(processor(ctx))
					.build();

			//@formatter:on
			client.connect();
			running = true;

			while (running) {
				synchronized (waitLock) {
					waitLock.wait(100L);
				}
			}
		}

		private HosebirdMessageProcessor processor(SourceContext<String> ctx) {
			return new HosebirdMessageProcessor() {
				
				DelimitedStreamReader reader;

				@Override
				public void setup(InputStream input) {
					reader = new DelimitedStreamReader(input, UTF_8, 50000);
				}

				@Override
				public boolean process() throws IOException, InterruptedException {
					String line = reader.readLine();
					ctx.collect(line);
					return true;
				}
			};
		}

		@Override
		public void cancel() {
			close();
		}

		@Override
		public void stop() {
			close();
		}

		@Override
		public void close() {
			running = false;

			if (client != null) {
				client.stop();
			}

			synchronized (waitLock) {
				waitLock.notify();
			}

		}

	}

}
