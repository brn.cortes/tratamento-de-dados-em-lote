package br.com.unisul.atividade;

import java.util.Arrays;
import java.util.Collections;

import org.apache.flink.api.common.functions.StoppableFunction;
import org.apache.flink.api.common.io.GlobFilePathFilter;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.io.TextInputFormat;
import org.apache.flink.api.java.operators.FlatMapOperator;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.core.fs.Path;
import org.apache.flink.streaming.api.functions.source.RichSourceFunction;

public class HdfsSource extends RichSourceFunction<String> implements StoppableFunction { 

	private transient Object waitLock;

	private transient boolean running = true;

	private static final long serialVersionUID = 7284963727210671225L;

	@Override
	public void open(Configuration parameters) throws Exception {
		waitLock = new Object();
	}

	@Override
	public void run(SourceContext<String> ctx) throws Exception {			
		String path = "hdfs://localhost:9000/Atividade/input";
		ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();
		TextInputFormat format = new TextInputFormat(new Path(path));

		GlobFilePathFilter filesFilter = new GlobFilePathFilter(
				Collections.singletonList("**"),
				Arrays.asList("**/*.in-progress"));

		format.setFilesFilter(filesFilter);
		FlatMapOperator<String, Object> flatMap = env.readFile(format, path)
			.filter(value -> !value.trim().isEmpty())
			.flatMap((value, out) -> out.collect(value))
			.returns(Object.class);
		ctx.collect(flatMap.collect().toString());

		running = true;

		while (running) {
			synchronized (waitLock) {
				waitLock.wait(100L);
			}
		}
	}

	@Override
	public void cancel() {
		close();
	}

	@Override
	public void stop() {
		close();
	}

	@Override
	public void close() {
		running = false;

		synchronized (waitLock) {
			waitLock.notify();
		}

	}
}
